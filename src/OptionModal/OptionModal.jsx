import React, { Component } from "react";

// localhost chrome wont vibrate

class OptionModal extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleOptions(value) {
    this.props.handleOptions(value);
  }

  toggleModal() {
    this.props.toggleModal();
  }

  render() {
    return (
      <div className={`optionmodal ${this.props.openModal ? "open" : "closed"}`}>
        <div className="options">
          {this.props.options.map(option => {
            return (
              <div className="option" onClick={this.handleOptions.bind(this, option.value)}>
                {option.option}
              </div>
            );
          })}
          <div className="option close" onClick={this.toggleModal.bind(this)}>
            Close
          </div>
        </div>
      </div>
    );
  }
}

export default OptionModal;
