import React, { Component } from "react";
import { handleTouch,vibrate } from "../Utils/Utils";

// localhost chrome wont vibrate

class SwipeToDelete extends Component {
  constructor() {
    super();
    this.state = {
      swipeDirection: ""
    };
  }
  handleTouchEnd(e) {
    let swipeDirection = handleTouch.handleTouchEnd(e);
    if (swipeDirection !== this.state.swipeDirection) {
      let oldState = Object.assign({}, this.state);
      oldState.swipeDirection = swipeDirection;
      this.setState(oldState, vibrate(20));
    }
  }
  handleTouchStart(e) {
    handleTouch.handleTouchStart(e);
  }

  render() {
    return (
      <div className="swipetooptions" onTouchStart={this.handleTouchStart} onTouchMove={this.handleTouchEnd.bind(this)} ref="swipetooptions">
        <div className="body" ref={this.body}>
          swipe for options
        </div>
        <div className={`options ${this.state.swipeDirection === 'left' ? 'visible' : 'hidden'}`}>
          <div className="edit">DELETE</div>
          <div className="delete">EDIT</div>
        </div>
      </div>
    );
  }
}

export default SwipeToDelete;
