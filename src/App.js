import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import "./SwipeToDelete/swipetodelete.scss";
import "./OptionModal/optionmodal.scss";
import SwipeToDelete from "./SwipeToDelete/SwipeToDelete";
import OptionModal from "./OptionModal/OptionModal";

class App extends Component {
  constructor() {
    super();
    this.state = {
      bodyBlur: false,
      openModal: false
    };
  }

  toggleBlur() {
    let oldState = Object.assign({}, this.state);
    oldState.bodyBlur = !this.bodyBlur;
    this.setState(oldState);
  }
  toggleModal() {
    let oldState = Object.assign({}, this.state);
    oldState.openModal = !this.state.openModal;
    this.setState(oldState);
  }

  handleOptions(option) {
    alert(option);
  }

  render() {
    let options = [
      {
        option : 'one',
        value : 'one'
      },
      {
        option : 'two',
        value : 'two'
      },
      {
        option : 'three',
        value : 'three'
      },
    ];
    return (
      <div className={`App ${this.state.bodyBlur ? "blur" : ""}`}>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <SwipeToDelete />
          <button onClick={this.toggleModal.bind(this)}>Open option modal</button>
          <OptionModal
            toggleModal={this.toggleModal.bind(this)}
            openModal={this.state.openModal}
            handleOptions={this.handleOptions.bind(this)}
            options={options}
          />
        </header>
      </div>
    );
  }
}

export default App;
