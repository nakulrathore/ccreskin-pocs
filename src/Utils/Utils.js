class TouchEvents {
  //trying class here
  constructor() {
    this.touchstartX = 0;
    this.touchstartY = 0;
    this.touchendX = 0;
    this.touchendY = 0;
    this.swipeDirection = "";
  }

  handleTouchStart(event) {
    this.touchstartX = event.changedTouches[0].clientX;
    this.touchstartY = event.changedTouches[0].clientY;
  }
  handleTouchEnd(event) {
    this.touchendX = event.changedTouches[0].clientX;
    this.touchendY = event.changedTouches[0].clientY;
    if (this.touchendX <= this.touchstartX) {
      return "left";
    }
    if (this.touchendX >= this.touchstartX) {
      return "right";
    }
  }
}
export let handleTouch = new TouchEvents();


export function vibrate(value) {
  //we can define vib standard and allow only value options like [alert/action/success/megaWarning etc]

  if ("vibrate" in navigator) {
    navigator.vibrate(value);
  } else {
    console.log("vibrator not found");
  }
}
